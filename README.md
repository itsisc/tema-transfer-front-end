# Tema transfer Front End

Salutare salutare! 

Ne bucurăm că dorești să devii un crocodil de nădejde care se va lupta cu codul! 

Însă în calea ta mai este un singur pas de trecut. Acesta este tema individuală care se află în fișier. 

În acel fișier se găsesc toate pozele folosite în site, dar și pdf-ul care are rol de mock-up, pe baza căruia vei face site-ul. 

În caz de probleme/nelămuriri poți să apelezi la mine(mihneacatana@gmail.com) sau la Alexandru Stănică (stanica.alexandru99@gmail.com)

Tema o vei trimite la adresa **mihneacatana@gmail.com** , iar în CC: **stanica.alexandru99@gmail.com**

Pentru a putea să treceți și peste acest pas o să aveți nevoie de:
- [Visual Studio Code](https://code.visualstudio.com)
- [Git](https://git-scm.com)

De asemenea, vă lăsăm aici niște materiale ce vă pot fi de mare ajutor!

-  [HTML & CSS](https://www.khanacademy.org/computing/computer-programming/html-css) pentru a trece prin toate noțiunile de bază, până la secțiunea  Web development tools.
-  [JavaScript](http://jsforcats.com) pentru noțiuni de bază și introductive în minunatele lumi ale JS-ului.
-  [Fontul](https://fonts.google.com/specimen/Glory) folosit în realizarea site-ului

Extra: Site-ul să fie [responsive](https://www.w3schools.com/css/css_rwd_mediaqueries.asp) 


Mentiuni: 
Navbarul apare in cele toate 3 paginile deoarece este unul sticky, ce nu dispare la scroll. 
Butonul "See the gallery" trimite la carusel.

Mult succes în realizarea ei! 
